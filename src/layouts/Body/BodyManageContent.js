import React, { useState, useEffect, useCallback } from "react";
import "./BodyManage.scss";
import {
  Box,
  Button,
  Typography,
  Paper,
  Stack,
  TextField,
  MenuItem,
  Select,
  Modal,
  useTheme,
  useMediaQuery,
} from "@mui/material";
import {
  Delete,
  DriveFileRenameOutlineOutlined,
  InfoOutlined,
} from "@mui/icons-material";
import { useForm, Controller } from "react-hook-form";
import {
  DataGrid,
  GridToolbar,
  gridPageCountSelector,
  GridPagination,
  useGridApiContext,
  useGridSelector,
} from "@mui/x-data-grid";
import MuiPagination from "@mui/material/Pagination";
import { useTranslation } from "react-i18next";
import Input from "../../components/Input/Input";
import {
  changePassword,
  connectLMSApi,
  deleteContent,
  getContentClient,
  getInfoAccount,
} from "../../api";
import parseJwt from "../../funtions/parseJwt";
import { useNavigate } from "react-router-dom";
import TimeVN from "../../funtions/timeVN";
import EmptyFolder from "../EmptyFolder/EmptyFolder";
import { toast } from "react-toastify";
import handleErrorFunction from "../../funtions/handleErrorFunction";
import { common } from "../../utils/common";
import axios from "axios";
import { useAppContext } from "../../context/AppContext";

function Pagination({ page, onPageChange, className }) {
  const apiRef = useGridApiContext();
  const pageCount = useGridSelector(apiRef, gridPageCountSelector);

  return (
    <MuiPagination
      color="primary"
      className={className}
      count={pageCount}
      page={page + 1}
      onChange={(event, newPage) => {
        onPageChange(event, newPage - 1);
      }}
    />
  );
}

function CustomPagination(props) {
  return <GridPagination ActionsComponent={Pagination} {...props} />;
}

function DetailPanelContent({ row }) {
  const { t } = useTranslation("translation", {
    keyPrefix: "layouts.Body.BodyManageContent",
  });
  const apiRef = useGridApiContext();
  const {
    control,
    handleSubmit,
    formState: { isValid },
  } = useForm({
    defaultValues: row,
    mode: "onChange",
  });

  const onSubmit = (data) => {
    apiRef.current.updateRows([data]);
    apiRef.current.toggleDetailPanel(row.id);
  };

  return (
    <Stack
      sx={{ py: 2, height: "100%", boxSizing: "border-box" }}
      direction="column"
    >
      <Paper sx={{ flex: 1, mx: "auto", width: "90%", p: 1 }}>
        <Stack
          component="form"
          justifyContent="space-between"
          onSubmit={handleSubmit(onSubmit)}
          sx={{ height: 1, maxWidth: 400, gap: 3 }}
        >
          <Typography variant="h6">{`Edit ID #${row.id}`}</Typography>
          <Controller
            control={control}
            name="name"
            rules={{ required: true }}
            render={({ field, fieldState: { invalid } }) => (
              <TextField
                label="Name"
                size="small"
                error={invalid}
                required
                fullWidth
                {...field}
              />
            )}
          />
          <Controller
            control={control}
            name="role"
            rules={{ required: true }}
            render={({ field, fieldState: { invalid } }) => (
              <Select
                label="Role"
                size="small"
                error={invalid}
                required
                fullWidth
                {...field}
              >
                <MenuItem value="Administrator">Administrator</MenuItem>
                <MenuItem value="Super user">Super user</MenuItem>
                <MenuItem value="Author">Author</MenuItem>
              </Select>
            )}
          />
          <div>
            <Button
              type="submit"
              variant="outlined"
              size="small"
              disabled={!isValid}
            >
              {t("save")}
            </Button>
          </div>
        </Stack>
      </Paper>
    </Stack>
  );
}

export default function BodyContent({ allContent, myContent, collabContent }) {
  const navigate = useNavigate();
  const { t } = useTranslation("translation", {
    keyPrefix: "layouts.Body.BodyManageContent",
  });
  const v = useTranslation("translation", {
    keyPrefix: "validates",
  });
  const {
    userInfo: { accountID, id },
  } = useAppContext();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("sm"));
  const VISIBLE_FIELDS = matches
    ? ["title", "owner", "lastModified", "actions"]
    : ["title", "owner", "actions"];

  const [contentDelete, setContentDelete] = useState({
    id: "",
    title: "",
  });
  const [rows, setRows] = useState("");
  const [selectedRow, setSelectedRow] = useState({
    title: "",
    owner: "",
    lastModified: "",
    lastModifiedBy: "",
    feedback: "",
    published: "",
  });
  const [contentType, setContentType] = useState([]);
  const [loading, setLoading] = useState(false);
  const [openModalDetailRows, setOpenModalDetailRows] = React.useState(false);
  const [openModalDeleteContent, setOpenModalDeleteContent] = useState(false);
  const [htmlContent, setHtmlContent] = useState("");
  const [parseDataJwt, setParseDataJwt] = useState(null);

  const columns = [
    { field: "id", headerName: t("STT"), flex: 1 },
    { field: "title", headerName: t("title"), flex: 1 },

    {
      field: "owner",
      headerName: t("owner"),
      flex: 1,
    },
    {
      field: "lastModified",
      headerName: t("lastModified"),
      flex: 1,
    },
    {
      field: "lastModifiedBy",
      headerName: t("lastModifiedBy"),
      flex: 1,
    },
    {
      field: "feedback",
      headerName: t("feedback"),
      flex: 1,
    },
    {
      field: "published",
      headerName: t("published"),
      flex: 1,
    },
    {
      field: "actions",
      headerName: t("actions"),
      flex: 1,
      renderCell: (params) => (
        <Box sx={{ display: "flex", gap: 4 }}>
          {common.getLtiLMS() && (
            <Button
              size="small"
              variant="contained"
              color="success"
              onClick={() => handleInsert(params.row)}
            >
              {t("insert")}
            </Button>
          )}
          <DriveFileRenameOutlineOutlined
            style={{ cursor: "pointer" }}
            onClick={() => handleEdit(params.row.id)}
          />
          <Delete
            style={{ cursor: "pointer" }}
            onClick={() => handleDelete(params.row.id, params.row.title)}
          />
          {/* <InfoOutlined
            style={{ cursor: "pointer" }}
            onClick={() => handleDetail(params.row)}
          /> */}
        </Box>
      ),
    },
  ];

  const handleEdit = (content_id) => {
    navigate(`/content/edit?id=${content_id}`);
  };

  const handleInsert = async (row) => {
    const response = await connectLMSApi.selectContent(row.id);
    document.open();
    document.write(response.data);
    document.close();
  };

  const handleDelete = (contentId, title) => {
    handleOpenModalDeleteContent();
    setContentDelete({ id: contentId, title: title });
  };

  const loadData = useCallback(async () => {
    if (!accountID) return;
    try {
      //Lấy ngôn ngữ hiện tại của tổ chức
      const responseLanguage = await getInfoAccount(accountID);
      const infoAccountData = parseJwt(responseLanguage.data.jwt);
      const language = infoAccountData.defaultLocale;

      const res = await getContentClient(id);
      const jwt = res.data.jwt;
      const json = parseJwt(jwt);

      let contentType2;

      if (collabContent === true) {
        setContentType(json.collab_contents);
        contentType2 = json.collab_contents;
      } else if (myContent === true) {
        setContentType(json.my_contents);
        contentType2 = json.my_contents;
      } else if (allContent === true) {
        if (json.my_contents.length > 0 && json.collab_contents.length > 0) {
          json.my_contents.forEach((content) => {
            json.collab_contents.push(content);
          });

          json.all_contents = json.collab_contents;
        } else if (
          json.my_contents.length === 0 &&
          json.collab_contents.length > 0
        ) {
          json.all_contents = [...json.collab_contents];
        } else if (
          json.my_contents.length > 0 &&
          json.collab_contents.length === 0
        ) {
          json.all_contents = [...json.my_contents];
        } else {
          json.all_contents = [];
        }
        setContentType(json.all_contents);
        contentType2 = json.all_contents;
      }
      const convertedArray = contentType2.map((obj) => {
        const published = obj.workflowState === "active" ? "public" : "";
        return {
          id: obj.mongoId,
          title: obj.settings.metadata.title,
          owner: obj.clients[0].fullName,
          lastModified:
            language === "vi" ? TimeVN(obj.updatedAt) : obj.updateAt,
          lastModifiedBy: "",
          feedback: "",
          published: published,
          createdAt: obj.createdAt,
        };
      });
      setRows(convertedArray);
      setLoading(true);
    } catch (error) {
      toast.error(handleErrorFunction(error, v));
    }
  }, [accountID]);

  const handleDeleteContent = async () => {
    try {
      const res = await deleteContent(contentDelete.id);
      if (res) {
        if (res.status === 200) {
          toast.success(t("delete_content_successfully"));
          loadData();
        } else {
          toast.error(t("this_content_cannot_be_deleted"));
        }
        handleCloseModalDeleteContent();
      }
    } catch (error) {
      toast.error(handleErrorFunction(error, t));
      handleCloseModalDeleteContent();
    }
  };

  const handleDetail = (row) => {
    handleOpenModalDetailRows();
    setSelectedRow({
      title: row.title,
      owner: row.owner,
      lastModified: row.lastModified,
      lastModifiedBy: row.lastModifiedBy,
      feedback: row.feedback,
      published: row.published,
    });
  };

  const columnss = React.useMemo(
    () => columns.filter((column) => VISIBLE_FIELDS.includes(column.field)),
    [columns]
  );

  const getDetailPanelContent = React.useCallback(
    ({ row }) => <DetailPanelContent row={row} />,
    []
  );
  const getDetailPanelHeight = React.useCallback(() => 240, []);

  const handleOpenModalDetailRows = () => setOpenModalDetailRows(true);
  const handleCloseModalDetailRows = () => setOpenModalDetailRows(false);

  const handleOpenModalDeleteContent = () => setOpenModalDeleteContent(true);
  const handleCloseModalDeleteContent = () => {
    setOpenModalDeleteContent(false);
    setContentDelete({ id: "", title: "" });
  };

  const handleRowClick = (row) => {
    navigate(`/content/view?id=${row.id}`);
  };

  useEffect(() => {
    loadData();
  }, [loadData]);

  return (
    <>
      {loading === true && (
        <>
          {contentType.length === 0 ? (
            <>
              {allContent === true ? (
                <EmptyFolder type="allContent" />
              ) : myContent === true ? (
                <EmptyFolder type="myContent" />
              ) : (
                <EmptyFolder type="shareContent" />
              )}
            </>
          ) : (
            <div className="bodymanage_layout">
              <div className="bodymanage_table">
                <DataGrid
                  rows={rows}
                  columns={columnss}
                  pagination
                  rowThreshold={0}
                  getDetailPanelHeight={getDetailPanelHeight}
                  getDetailPanelContent={getDetailPanelContent}
                  slots={{
                    pagination: CustomPagination,
                    toolbar: GridToolbar,
                  }}
                  localeText={{
                    toolbarColumns: t("columns"),
                    toolbarFilters: t("filter"),
                    toolbarDensity: t("density"),
                    toolbarExport: t("export"),
                  }}
                  slotProps={{
                    toolbar: {
                      showQuickFilter: true,
                      quickFilterProps: { debounceMs: 500 },
                    },
                  }}
                  {...rows}
                  initialState={{
                    ...rows.initialState,
                    pagination: { paginationModel: { pageSize: 25 } },
                  }}
                  pageSizeOptions={[5, 10, 15, 20, 25]}
                  checkboxSelection
                  disableRowSelectionOnClick
                  onCellClick={(params, event) => {
                    const { field } = params.colDef;
                    if (
                      field === "lastModified" ||
                      field === "owner" ||
                      field === "title"
                    ) {
                      handleRowClick(params);
                    }
                  }}
                  className="grid"
                />
              </div>

              <Modal
                open={openModalDeleteContent}
                onClose={handleCloseModalDeleteContent}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
              >
                <Box
                  className="Box-Modal"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 20,
                  }}
                >
                  <div>
                    {t("are_you_sure_you_want_to_delete_content")}
                    <br />
                    <Typography variant="h5">
                      {contentDelete.title} ?
                    </Typography>
                  </div>
                  <Box sx={{ display: "flex", gap: 2 }}>
                    <Button
                      variant="contained"
                      size="large"
                      className="button"
                      onClick={handleDeleteContent}
                    >
                      {t("delete")}
                    </Button>
                    <Button
                      size="large"
                      className="button"
                      style={{ color: "#3c4859" }}
                      onClick={handleCloseModalDeleteContent}
                    >
                      {t("cancel")}
                    </Button>
                  </Box>
                </Box>
              </Modal>

              <Modal
                open={openModalDetailRows}
                onClose={handleCloseModalDetailRows}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
              >
                <Box
                  className="Box-Modal"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 20,
                  }}
                >
                  <Input
                    label={t("title")}
                    value={selectedRow.title}
                    name="title"
                    disabled="disabled"
                  />
                  <Input
                    label={t("owner")}
                    value={selectedRow.owner}
                    name="owner"
                    disabled="disabled"
                  />
                  <Input
                    label={t("lastModified")}
                    value={selectedRow.lastModified}
                    name="lastModified"
                    disabled="disabled"
                  />
                  <Input
                    label={t("lastModifiedBy")}
                    value={selectedRow.lastModifiedBy}
                    name="lastModifiedBy"
                    disabled="disabled"
                  />
                  <Input
                    label={t("feedback")}
                    value={selectedRow.feedback}
                    name="feedback"
                    disabled="disabled"
                  />
                  <Input
                    label={t("published")}
                    value={selectedRow.published}
                    name="published"
                    disabled="disabled"
                  />

                  <Box sx={{ display: "flex" }}>
                    <Button
                      size="large"
                      className="button cancel-button"
                      style={{ color: "#3c4859" }}
                      onClick={handleCloseModalDetailRows}
                    >
                      {t("close")}
                    </Button>
                  </Box>
                </Box>
              </Modal>
            </div>
          )}
        </>
      )}
    </>
  );
}
