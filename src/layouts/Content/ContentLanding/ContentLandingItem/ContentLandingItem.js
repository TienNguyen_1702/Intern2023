import React from "react";
import "./ContentLandingItem.scss";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";


export default function ContentLandingItem({ title, content, video, option }) {
  return (
    <div
      className={
        option === "reserve"
          ? "contentlandingitem_container"
          : "contentlandingitem_container contentlandingitem_container-reserve"
      }
    >
      <div className="contentlandingitem_item contentlandingitem_item1">
        <div>
          <AddCircleOutlineIcon />
        </div>
        <div className="title">{title}</div>
        <div className="content">{content}</div>
      </div>
      <div
        className={
          option === "reserve"
            ? "contentlandingitem_item contentlandingitem_item2"
            : "contentlandingitem_item contentlandingitem_item2-reserve"
        }
      >
        
        {/* <video width="320" height="240" autoplay muted playsinline>
          <source src={video} type="video/mp4" />
        </video> */}
      </div>
    </div>
  );
}
