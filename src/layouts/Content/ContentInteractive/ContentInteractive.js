import React from "react";

import ContentInteractiveItem from "./ContentInteractiveItem/ContentInteractiveItem";
import "./ContentInteractive.scss";

export default function ContentInteractive({ listContentInteractive }) {
  return (
    <div className="contentinteractive_layout">
      {listContentInteractive.map((item) => {
        return (
          <ContentInteractiveItem
            video={item["video"]}
            content={item["content"]}
            title={item["title"]}
            buttonTitle={item["buttonTitle"]}
          />
        );
      })}
    </div>
  );
}
