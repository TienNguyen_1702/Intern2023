import React from "react";
import { Button } from "@mui/material";

import "./ContentInteractiveItem.scss";

export default function ContentInteractiveItem({
  video,
  title,
  content,
  buttonTitle,
}) {
  return (
    <div className="contentinteractiveitem_layout">
      <div className="contentinteractiveitem_body">
        <div className="video">
          <video width="300" height="220" autoplay muted playsinline>
            <source src={video} type="video/mp4" />
          </video>
        </div>
        <div className="title">{title}</div>
        <div className="content-button">
          <div className="content">{content}</div>
          {buttonTitle !== "" ? (
            <div className="button">
              <Button variant="contained" size="large" className="button">
                {buttonTitle}
              </Button>
            </div>
          ) : (
            <></>
          )}
        </div>
      </div>
    </div>
  );
}
