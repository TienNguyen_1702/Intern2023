import {
  AccountCircle,
  ArrowDropDown,
  ChangeCircleOutlined,
  LogoutOutlined,
  NotificationsActiveOutlined,
  Person,
} from "@mui/icons-material";
import { Button, Divider, MenuItem } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { useAppContext } from "../../context/AppContext";
import { common } from "../../utils/common";
import StyledMenu from "../StyledMenu/StyledMenu";
import "./Menu.scss";

export default function CustomizedMenus() {
  const { userInfo } = useAppContext();
  const navigate = useNavigate();
  const { t } = useTranslation("translation", { keyPrefix: "layouts.Menu" });
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    const JWT_loginToken = common.getToken();
    if (!JWT_loginToken) {
      toast.error("Phiên đăng nhập đã hết hạn");
      navigate("/login");
    }
  }, [navigate]);

  const handleLogout = () => {
    setAnchorEl(null);
    // localStorage.removeItem("JWT_loginToken");
    // localStorage.removeItem("email");
    localStorage.clear();
    navigate("/login");
  };

  return (
    <div className="menu_component">
      <Button
        id="demo-customized-button"
        aria-controls={open ? "demo-customized-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        variant="contained"
        disableElevation
        onClick={handleClick}
        startIcon={<AccountCircle />}
        endIcon={<ArrowDropDown />}
        size="large"
        className="menu_button"
      >
        {userInfo?.fullName}
      </Button>
      <StyledMenu
        id="demo-customized-menu"
        MenuListProps={{
          "aria-labelledby": "demo-customized-button",
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose} disableRipple>
          {userInfo?.email}
        </MenuItem>

        <Divider sx={{ my: 0.5 }} />
        {/* <Link
          to="https://help.h5p.com/hc/en-us/articles/7633089307677"
          class="link"
        >

          <MenuItem onClick={handleClose} disableRipple>
            <HelpOutline />
            {t("getting_started")}
          </MenuItem>
        </Link> */}
        <Link to="/announcements" className="link">
          <MenuItem onClick={handleClose} disableRipple>
            <NotificationsActiveOutlined />
            {t("announcements")}
          </MenuItem>
        </Link>
        <Link to="/account" className="link">
          <MenuItem onClick={handleClose} disableRipple>
            <Person />
            {t("my_account")}
          </MenuItem>
        </Link>
        <Link to="/change-password" className="link">
          <MenuItem onClick={handleClose} disableRipple>
            <ChangeCircleOutlined />
            {t("change_password")}
          </MenuItem>
        </Link>

        <MenuItem onClick={handleLogout} disableRipple>
          <LogoutOutlined />
          {t("logout")}
        </MenuItem>
      </StyledMenu>
    </div>
  );
}
