import React from "react";
import "./Dropdown.scss";

export default function Select({
  label,
  listItem,
  value,
  name,
  onChange,
  disabled,
}) {
  return (
    <>
      <div className="input_component">
        <label htmlFor="dropdown" className="label">
          {label}
        </label>
        <select
          id="dropdown"
          className="input"
          value={value}
          name={name}
          onChange={onChange}
          disabled={disabled}
        >
          {listItem.map((item) => (
            <option key={item.id} value={item.id}>
              {item.name}
            </option>
          ))}
        </select>
      </div>
    </>
  );
}
