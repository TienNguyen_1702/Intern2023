import React from "react";
import HomeLayout from "../../../layouts/HomeLayout/HomeLayout";
import EmptyFolder from "../../../layouts/EmptyFolder/EmptyFolder";
import "./TrashPage.scss";
import { useTranslation } from "react-i18next";

const data = [];

const Trash = () => {
  const { t } = useTranslation("translation", {
    keyPrefix: "pages.HomePageContent.TrashPage",
  });
  return (
    <div className="homepagecontent_layout">
      <div className="homepagecontent_top">{t("trash_content")}</div>

      {data.length > 0 ? (
        <div className="trash_bottom"></div>
      ) : (
        <EmptyFolder type="trashContent" />
      )}
    </div>
  );
};

export default function TrashPage() {
  return (
    <div>
      <HomeLayout bodyManage={<Trash />} type="Manage Content" />
    </div>
  );
}
