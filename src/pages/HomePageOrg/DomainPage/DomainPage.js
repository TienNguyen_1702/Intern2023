import {
  Delete,
  DriveFileRenameOutlineOutlined,
  InfoOutlined,
} from "@mui/icons-material";
import {
  Box,
  Button,
  MenuItem,
  Modal,
  Paper,
  Select,
  Stack,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import MuiPagination from "@mui/material/Pagination";
import {
  DataGrid,
  GridPagination,
  GridToolbar,
  gridPageCountSelector,
  useGridApiContext,
  useGridSelector,
} from "@mui/x-data-grid";
import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import { deleteUser, getDomain, updateDomain } from "../../../api";
import Dropdown from "../../../components/Dropdown/Dropdown";
import Input from "../../../components/Input/Input";
import { useAppContext } from "../../../context/AppContext";
import handleErrorFunction from "../../../funtions/handleErrorFunction";
import parseJwt from "../../../funtions/parseJwt";
import HomeLayout from "../../../layouts/HomeLayout/HomeLayout";
import "./DomainPage.scss";

function Pagination({ page, onPageChange, className }) {
  const apiRef = useGridApiContext();
  const pageCount = useGridSelector(apiRef, gridPageCountSelector);

  return (
    <MuiPagination
      color="primary"
      className={className}
      count={pageCount}
      page={page + 1}
      onChange={(event, newPage) => {
        onPageChange(event, newPage - 1);
      }}
    />
  );
}

function CustomPagination(props) {
  return <GridPagination ActionsComponent={Pagination} {...props} />;
}

function DetailPanelContent({ row }) {
  const apiRef = useGridApiContext();
  const {
    control,
    handleSubmit,
    formState: { isValid },
  } = useForm({
    defaultValues: row,
    mode: "onChange",
  });

  const onSubmit = (data) => {
    apiRef.current.updateRows([data]);
    apiRef.current.toggleDetailPanel(row.id);
  };

  return (
    <Stack
      sx={{ py: 2, height: "100%", boxSizing: "border-box" }}
      direction="column"
    >
      <Paper sx={{ flex: 1, mx: "auto", width: "90%", p: 1 }}>
        <Stack
          component="form"
          justifyContent="space-between"
          onSubmit={handleSubmit(onSubmit)}
          sx={{ height: 1, maxWidth: 400, gap: 3 }}
        >
          <Typography variant="h6">{`Edit ID #${row.id}`}</Typography>
          <Controller
            control={control}
            name="name"
            rules={{ required: true }}
            render={({ field, fieldState: { invalid } }) => (
              <TextField
                label="Name"
                size="small"
                error={invalid}
                required
                fullWidth
                {...field}
              />
            )}
          />
          <Controller
            control={control}
            name="role"
            rules={{ required: true }}
            render={({ field, fieldState: { invalid } }) => (
              <Select
                label="Role"
                size="small"
                error={invalid}
                required
                fullWidth
                {...field}
              >
                <MenuItem value="Administrator">Administrator</MenuItem>
                <MenuItem value="Super user">SuperUser</MenuItem>
                <MenuItem value="Author">Author</MenuItem>
              </Select>
            )}
          />
          <div>
            <Button
              type="submit"
              variant="outlined"
              size="small"
              disabled={!isValid}
            >
              Save
            </Button>
          </div>
        </Stack>
      </Paper>
    </Stack>
  );
}

const Domain = () => {
  const {
    userInfo: { accountID },
  } = useAppContext();
  const { t } = useTranslation("translation", {
    keyPrefix: "pages.HomePageOrg.DomainPage",
  });

  const v = useTranslation("translation", {
    keyPrefix: "validates",
  }).t;

  const columns = [
    { field: "domain", headerName: t("domain"), flex: 6 },

    {
      field: "subdomain",
      headerName: t("subdomain"),
      flex: 5,
    },
    {
      field: "host",
      headerName: t("host"),
      flex: 5,
    },
    {
      field: "primary",
      headerName: t("primary"),
      flex: 5,
    },
    {
      field: "actions",
      headerName: "actions",
      flex: 5,
      renderCell: (params) => (
        <Box sx={{ display: "flex", gap: 3 }}>
          <DriveFileRenameOutlineOutlined
            style={{ cursor: "pointer" }}
            onClick={() => handleEdit(params.row)}
          />
          <Delete
            style={{ cursor: "pointer" }}
            onClick={() => handleDelete(params.row)}
          />
          <InfoOutlined
            style={{ cursor: "pointer" }}
            onClick={() => handleDetail(params.row)}
          />
        </Box>
      ),
    },
  ];

  const [rows, setRows] = useState([]);
  const [rowDelete, setRowDelete] = useState("");

  const [selectedRow, setSelectedRow] = useState({
    id: "",
    domain: "",
    subdomain: "",
    host: "",
    primary: "",
  });

  const [openModalEditRows, setOpenModalEditRows] = useState(false);
  const handleOpenModalEditRows = () => setOpenModalEditRows(true);
  const handleCloseModalEditRows = () => setOpenModalEditRows(false);

  const [openModalDetailRows, setOpenModalDetailRows] = useState(false);
  const handleOpenModalDetailRows = () => setOpenModalDetailRows(true);
  const handleCloseModalDetailRows = () => setOpenModalDetailRows(false);

  const [openModalDeleteRows, setOpenModalDeleteRows] = useState(false);
  const handleOpenModalDeleteRows = () => setOpenModalDeleteRows(true);
  const handleCloseModalDeleteRows = () => {
    setOpenModalDeleteRows(false);
    setRowDelete("");
  };

  const handleEdit = (row) => {
    handleOpenModalEditRows();
    setSelectedRow({
      id: row.id,
      domain: row.domain,
      subdomain: row.subdomain,
      host: row.host,
      primary: row.primary,
    });
  };

  const handleDelete = (row) => {
    handleOpenModalDeleteRows();
    setRowDelete(row);
  };

  const handleDetail = (row) => {
    handleOpenModalDetailRows();
    setSelectedRow({
      id: row.id,
      domain: row.domain,
      subdomain: row.subdomain,
      host: row.host,
      primary: row.primary,
    });
  };

  const mobile = useMediaQuery("(max-width:767px)");
  const VISIBLE_FIELDS =
    mobile === true
      ? ["domain", "subdomain", "host", "primary", "actions"]
      : ["domain", "subdomain", "host", "primary", "actions"];
  const loadData = async () => {
    try {
      const respond = await getDomain(accountID);

      const json = parseJwt(respond.data.jwt);

      const array = Object.entries(json)
        .filter(([key, value]) => key !== "iat" && key !== "exp")
        .map(([key, value]) => value);
      setRows(array);
    } catch (err) {}
  };

  useEffect(() => {
    loadData();
  }, []);

  const columnss = React.useMemo(
    () => columns.filter((column) => VISIBLE_FIELDS.includes(column.field)),
    [columns]
  );

  const getDetailPanelContent = React.useCallback(
    ({ row }) => <DetailPanelContent row={row} />,
    []
  );
  const getDetailPanelHeight = React.useCallback(() => 240, []);

  const handleChangeName = (event) => {
    const { name, value } = event.target;
    setSelectedRow({
      ...selectedRow,
      [name]: value,
    });
  };

  const handleDeleteDomain = async () => {
    try {
      const respond = await deleteUser(rowDelete, accountID);
      if (respond.status === 200) {
        handleCloseModalDeleteRows();
        toast.success(t("delete_domain_successfully"));
        loadData();
        setRowDelete("");
      }
    } catch (error) {
      toast.error(t("domain_deletion_failed"));
      handleCloseModalDeleteRows();
      setRowDelete("");
    }
  };

  const handleEditDomain = async () => {
    try {
      if (selectedRow.primary === "true" || selectedRow.primary === "") {
        selectedRow.primary = true;
      } else if (selectedRow.primary === "false") {
        selectedRow.primary = false;
      }
      const domainUpdate = {
        domain: selectedRow.domain,
        subdomain: selectedRow.subdomain,
        host: selectedRow.host,
        primary: selectedRow.primary,
      };
      const domain_id = selectedRow.id;
      const respond = await updateDomain(domainUpdate, domain_id);
      if (respond.status === 200) {
        toast.success(t("domain_update_successful"));
        handleCloseModalEditRows();
        loadData();
      }
    } catch (error) {
      const errorReturn = handleErrorFunction(error, v);
      toast.error("Không thể chỉnh sửa domain này");
    }
  };

  return (
    <div className="bodymanage_layout">
      <div className="bodymanage_table">
        <DataGrid
          rows={rows}
          columns={columnss}
          pagination
          rowThreshold={0}
          getDetailPanelHeight={getDetailPanelHeight}
          getDetailPanelContent={getDetailPanelContent}
          slots={{
            pagination: CustomPagination,
            toolbar: GridToolbar,
          }}
          localeText={{
            toolbarColumns: t("columns"),
            toolbarFilters: t("filter"),
            toolbarDensity: t("density"),
            toolbarExport: t("export"),
          }}
          slotProps={{
            toolbar: {
              showQuickFilter: true,
              quickFilterProps: { debounceMs: 500 },
            },
          }}
          {...rows}
          initialState={{
            ...rows.initialState,
            pagination: { paginationModel: { pageSize: 25 } },
          }}
          pageSizeOptions={[5, 10, 15, 20, 25]}
          checkboxSelection
          className="grid"
        />
      </div>

      <Modal
        open={openModalEditRows}
        onClose={handleCloseModalEditRows}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          className="Box-Modal"
          style={{ display: "flex", flexDirection: "column", gap: 20 }}
        >
          <Input
            label="Domain"
            value={selectedRow.domain}
            name="domain"
            onChange={handleChangeName}
          />
          <Input
            label="Subdomain"
            value={selectedRow.subdomain}
            name="subdomain"
            onChange={handleChangeName}
          />
          <Input
            label="Host"
            value={selectedRow.host}
            name="host"
            onChange={handleChangeName}
            disabled="disabled"
          />
          <Dropdown
            value={selectedRow.primary}
            listItem={[
              { id: 1, name: "true" },
              { id: 2, name: "false" },
            ]}
            label="Primary"
            name="primary"
            onChange={handleChangeName}
          />

          <Box sx={{ display: "flex", gap: 2 }}>
            <Button
              variant="contained"
              size="large"
              className="button"
              onClick={handleEditDomain}
            >
              {t("save")}
            </Button>
            <Button
              size="large"
              className="button"
              style={{ color: "#3c4859" }}
              onClick={handleCloseModalEditRows}
            >
              {t("cancel")}
            </Button>
          </Box>
        </Box>
      </Modal>
      <Modal
        open={openModalDetailRows}
        onClose={handleCloseModalDetailRows}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          className="Box-Modal"
          style={{ display: "flex", flexDirection: "column", gap: 20 }}
        >
          <Input
            label="Domain"
            value={selectedRow.domain}
            name="domain"
            disabled="disabled"
          />
          <Input
            label="Subdomain"
            value={selectedRow.subdomain}
            name="subdomain"
            disabled="disabled"
          />
          <Input
            label="Host"
            value={selectedRow.host}
            name="host"
            disabled="disabled"
          />
          <Input
            label="Primary"
            value={selectedRow.primary}
            name="primary"
            disabled="disabled"
          />

          <Box sx={{ display: "flex" }}>
            <Button
              size="large"
              className="button cancel-button"
              style={{ color: "#3c4859" }}
              onClick={handleCloseModalDetailRows}
            >
              {t("close")}
            </Button>
          </Box>
        </Box>
      </Modal>
      <Modal
        open={openModalDeleteRows}
        onClose={handleCloseModalDeleteRows}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          className="Box-Modal"
          style={{ display: "flex", flexDirection: "column", gap: 20 }}
        >
          <div>
            {t("are_you_sure_you_want_to_delete_domain")} {rowDelete.domain} ?
          </div>
          <Box sx={{ display: "flex", gap: 2 }}>
            <Button
              variant="contained"
              size="large"
              className="button"
              onClick={handleDeleteDomain}
            >
              {t("delete")}
            </Button>
            <Button
              size="large"
              className="button"
              style={{ color: "#3c4859" }}
              onClick={handleCloseModalDeleteRows}
            >
              {t("cancel")}
            </Button>
          </Box>
        </Box>
      </Modal>
    </div>
  );
};

export default function DomainPage() {
  return (
    <div>
      <HomeLayout
        addDomain={true}
        bodyManage={<Domain />}
        type="Manage Organization"
      />
    </div>
  );
}
