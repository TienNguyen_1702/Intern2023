import LinkOutlinedIcon from "@mui/icons-material/LinkOutlined";
import { Button } from "@mui/material";
import React, { memo, useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import { connectLMSApi } from "../../../../api";
import AlertDialog from "../../../../components/Dialog/Alerts";
import { CONNECTION_STATUS } from "../../../../constants/connectLMS";
import { useConnectLMSConext } from "../Context";

const EnableButton = memo(({ id }) => {
  const { t } = useTranslation("translation", {
    keyPrefix: "pages.HomePageOrg.ConnectLMSPage",
  });
  const { reloadPage } = useConnectLMSConext();
  const [isOpen, setIsOpen] = useState(false);

  const handleOnClick = useCallback(() => {
    setIsOpen(!isOpen);
  }, [isOpen]);

  const handleEnable = useCallback(async () => {
    if (id) {
      const payload = {
        data: {
          status: CONNECTION_STATUS.ENABLED,
        },
      };
      try {
        const response = await connectLMSApi.updateConnectionLMS(id, payload);
        if (response.status === 200) {
          handleOnClick();
          toast.success(t("enable.successText"));
          reloadPage();
        }
      } catch (err) {
        console.error(err);
      }
    }
  }, [handleOnClick, id, reloadPage, t]);

  return (
    <>
      <Button
        variant="outlined"
        startIcon={<LinkOutlinedIcon />}
        onClick={handleOnClick}
      >
        Enable
      </Button>
      <AlertDialog
        open={isOpen}
        toggle={handleOnClick}
        handleSubmit={handleEnable}
        title={t("enable.title")}
        desc={t("enable.desc")}
      />
    </>
  );
});

export default EnableButton;
