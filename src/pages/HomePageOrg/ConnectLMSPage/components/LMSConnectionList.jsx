import React, { memo } from "react";
import { useConnectLMSConext } from "../Context";
import LMSItem from "./LMSItem";

const fakeData = [
  {
    type: "blackboard",
    tutorial_link: "https://help.h5p.com/hc/en-us/articles/7505840839325",
    name: "Blackboard",
    keyLti: "huong01-72838",
    secret: "VXguO10TQNxUoqCBBm2cTQdXVW75LY2w",
    status: "Enabled",
    last_updated: "2024-01-24 08:07:46",
    update_names:
      "Always update the user's name with the one provided by this LMS",
    update_emails:
      " Always update the user's e-mail address with the one provided by this LMS",
  },
  {
    type: "canvas",
    tutorial_link: "https://help.h5p.com/hc/en-us/articles/7507920890653",
    name: "Canvas test 02",
    // help: "Follow this step by step tutorial to set up LTI in Canvas",
    keyLti: "huong01-1d2f3",
    secret: "mjstaYIRmqcFAoN6qX2U6hodKNLALsUd",
    status: "Enabled",
    last_updated: "2024-01-24 07:33:46",
    update_names:
      "Always update the user's name with the one provided by this LMS",
    update_emails:
      " Always update the user's e-mail address with the one provided by this LMS",
  },
  {
    // Other with LTI version 1.1
    type: "other",
    name: "Other test 02",
    keyLti: "huong01-bc32b",
    secret: "Jqek7EI44SBTtcqUF4yV3Nx2zRA222Y5",
    status: "Enabled",
    last_updated: "2024-01-25 02:39:58",
    update_names:
      "Always update the user's name with the one provided by this LMS",
    update_emails:
      " Always update the user's e-mail address with the one provided by this LMS",
  },
];

const LMSConnectionList = memo(() => {
  const { connectLMSList } = useConnectLMSConext();

  return (
    <div className="connectLMS-list">
      {connectLMSList.map((item) => (
        <LMSItem key={item.key} keyLti={item.key} {...item} />
      ))}
    </div>
  );
});

export default LMSConnectionList;
