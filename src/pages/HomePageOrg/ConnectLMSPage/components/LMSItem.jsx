import WarningIcon from "@mui/icons-material/Warning";
import React, { memo } from "react";
import { LMS_TYPE, TUTORIAL_LINK } from "../../../../constants/connectLMS";
import { useTranslation } from "react-i18next";
import { common } from "../../../../utils/common";
import "../ConnectLMSPage.scss";
import KeyField from "../dataField/KeyField";
import SecretField from "../dataField/SecretField";
import UpdateEmails from "../dataField/UpdateEmails";
import UpdateNames from "../dataField/UpdateNames";
import LMSItemActions from "./LMSItemActions";

const LMSItem = memo(({ id, name, keyLti, secret, status, updatedAt, lm }) => {
  const { t } = useTranslation("translation", {
    keyPrefix: "pages.HomePageOrg.ConnectLMSPage.connectionItem",
  });

  return (
    <div className="connectLMS-list__item">
      <div className="header">
        <div className="lti-name">{name}</div>
        {lm?.type !== LMS_TYPE.OTHER && (
          <div className="lti-help">
            <WarningIcon />
            <span>
              {t("followThis")}
              <a
                href={TUTORIAL_LINK[lm?.type?.toLocaleUpperCase()]}
                target="_blank"
                rel="noreferrer"
              >
                {t("stepByStep")}
              </a>{" "}
              {t("setUpLti", { type: lm?.name })}
            </span>
          </div>
        )}
      </div>
      <div className="connection-fields">
        <table>
          <tbody>
            <tr>
              <th>{t("key")}</th>
              <KeyField value={keyLti} />
            </tr>
            <tr>
              <th>{t("secret")}</th>
              <SecretField value={secret} />
            </tr>
            <tr>
              <th>{t("status")}</th>
              <td>{common.capitalizeFirstLetter(status)}</td>
            </tr>
            <tr>
              <th>{t("lastUpdated")}</th>
              <td>{updatedAt}</td>
            </tr>
            <tr>
              <th>{t("updateNames.title")}</th>
              <td>
                <UpdateNames />
              </td>
            </tr>
            <tr>
              <th>{t("updateEmails.title")}</th>
              <td>
                <UpdateEmails />
              </td>
            </tr>
          </tbody>
        </table>
        <LMSItemActions id={id} status={status} />
      </div>
    </div>
  );
});

export default LMSItem;
