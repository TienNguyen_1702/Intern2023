import React, { useState, useEffect } from "react";
import Input from "../../components/Input/Input";
import { Button, Stack, Typography } from "@mui/material";
import { createPassword } from "../../api";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import "./Password.scss";

export default function EnterPassword() {
  const navigate = useNavigate();

  //Đa ngôn ngữ
  const { t } = useTranslation("translation", {
    keyPrefix: "pages.PasswordPage.EnterPasswordPage",
  });

  //Mật khẩu (enterPassword và confirmPassword)
  const [password, setPassword] = useState({
    enterPassword: "",
    confirmPassword: "",
  });

  const [checkPasswordMatch, setCheckPasswordMatch] = useState("");
  const [checkPasswordNull, setCheckPasswordNull] = useState("");
  const handleNewPasswordChange = (event) => {
    const { name, value } = event.target;
    setPassword({
      ...password,
      [name]: value,
    });
  };

  //Lấy pseudonym từ đường link truy cập bên email
  const [pseudonym, setPseudonym] = useState("");
  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const pseudonymValue = urlParams.get("confirmation");
    setPseudonym(pseudonymValue);
  }, []);

  //Gửi mật khẩu và pseudonym của account đó
  const handleCreatePassword = async () => {
    try {
      const respond = await createPassword(password, pseudonym);
      if (respond.status === 200) {
        toast.success(t("register_succcess"));
        navigate("/login");
      }
    } catch (error) {
      try {
        const errorRes = error.response.data.error;
        if (errorRes.status === 400) {
          if (errorRes.details.key === "not_match") {
            setCheckPasswordMatch(false);
            setCheckPasswordNull(true);
          } else if (errorRes.details.key === "invalid_input") {
            setCheckPasswordMatch(true);
            setCheckPasswordNull(false);
          }
        } else {
          toast.error(t("unable_to_set_password"));
        }
      } catch (error) {
        toast.error(t("connection_error"));
      }
    }
  };

  const handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      handleCreatePassword();
    }
  };

  return (
    <div className="password_page">
      <Stack spacing={2} className="password_layout">
        <Typography variant="h6" gutterBottom className="title">
          {t("set_up_password")}
        </Typography>

        <Input
          label={t("enter_password")}
          placeholder={t("enter_password")}
          type="password"
          id="enterPassword"
          name="enterPassword"
          value={password.enterPassword}
          onChange={handleNewPasswordChange}
          onKeyDown={handleKeyDown}
        />
        <Stack className="input-text-error">
          <Input
            label={t("confirm_password")}
            placeholder={t("confirm_password")}
            type="password"
            id="confirmPassword"
            name="confirmPassword"
            value={password.confirmPassword}
            onChange={handleNewPasswordChange}
            className={checkPasswordMatch === false ? "border-red" : ""}
            onKeyDown={handleKeyDown}
          />
          <div
            className={checkPasswordMatch === false ? "error-text" : "hidden"}
          >
            {t("password_does_not_match")}
          </div>
          <div
            className={checkPasswordNull === false ? "error-text" : "hidden"}
          >
            {t("missing_information_password")}
          </div>
        </Stack>
        <Button variant="contained" onClick={handleCreatePassword}>
          {t("set_up_password")}
        </Button>
      </Stack>
    </div>
  );
}
