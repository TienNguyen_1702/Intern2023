function convertURL (url) {
  url = url.replace("https://", "");
  url = url.replace("http://", "")
  return process.env.REACT_APP_NODE_ENV === 'production' ? `https://${url}` : `http://${url}`
}
export default convertURL;